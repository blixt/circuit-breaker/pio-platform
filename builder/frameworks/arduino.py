# Copyright 2014-present PlatformIO <contact@platformio.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Arduino

Arduino Wiring-based Framework allows writing cross-platform software to
control devices attached to a wide range of Arduino boards to create all
kinds of creative coding, interactive objects, spaces or physical experiences.

http://arduino.cc/en/Reference/HomePage
"""

from os.path import isdir, join

from SCons.Script import DefaultEnvironment

env = DefaultEnvironment()
platform = env.PioPlatform()
board = env.BoardConfig()
build_mcu = env.get("BOARD_MCU", board.get("build.mcu", ""))

FRAMEWORK_DIR = platform.get_package_dir("framework-arduinosam")
assert isdir(FRAMEWORK_DIR)
FRAMEWORK_VARIANT_BCB_DIR = platform.get_package_dir("framework-arduinosam-variant-bcb")
assert isdir(FRAMEWORK_VARIANT_BCB_DIR)

BUILD_CORE = board.get("build.core", "")
BUILD_SYSTEM = board.get("build.system", BUILD_CORE)
SYSTEM_DIR = join(FRAMEWORK_DIR, "system", BUILD_SYSTEM)

env.Append(
    ASFLAGS=["-x", "assembler-with-cpp"],

    CFLAGS=[
        "-std=gnu11"
    ],

    CCFLAGS=[
        "-Os",  # optimize for size
        "-ffunction-sections",  # place each function in its own section
        "-fdata-sections",
        "-Wall",
        "-mthumb",
        "-nostdlib",
        "--param", "max-inline-insns-single=500"
    ],

    CXXFLAGS=[
        "-fno-rtti",
        "-fno-exceptions",
        "-std=gnu++11",
        "-fno-threadsafe-statics"
    ],

    CPPDEFINES=[
        ("F_CPU", "$BOARD_F_CPU")#,
        #"USBCON" # Disable USB since we don't use USB on the MCU on the Circuit Breaker
    ],

    CPPPATH=[
        join(FRAMEWORK_DIR, "cores", BUILD_CORE)
    ],

    LINKFLAGS=[
        "-Os",
        "-mthumb",
        # "-Wl,--cref", # don't enable it, it prints Cross Reference Table
        "-Wl,--gc-sections",
        "-Wl,--check-sections",
        "-Wl,--unresolved-symbols=report-all",
        "-Wl,--warn-common",
        "-Wl,--warn-section-align"
    ],

    LIBPATH=[
        join(FRAMEWORK_DIR, "variants", board.get("build.variant"), "linker_scripts", "gcc"),
        join(FRAMEWORK_VARIANT_BCB_DIR, board.get("build.variant"), "linker_scripts", "gcc")
    ],

    LIBS=["m"]
)

env.Append(
    ASFLAGS=env.get("CCFLAGS", [])[:]
)

if "BOARD" in env:
    env.Append(
        CCFLAGS=[
            "-mcpu=%s" % board.get("build.cpu")
        ],
        LINKFLAGS=[
            "-mcpu=%s" % board.get("build.cpu")
        ]
    )


if BUILD_SYSTEM == "samd":
    env.Append(
            LINKFLAGS=[
            "--specs=nosys.specs",
            "--specs=nano.specs"
        ],
        
        CPPPATH=[
            join(SYSTEM_DIR, "CMSIS", "CMSIS", "Include"),
            join(SYSTEM_DIR, "CMSIS-Atmel", "CMSIS", "Device", "ATMEL")
        ],

        LIBPATH=[
            join(SYSTEM_DIR, "CMSIS", "CMSIS", "Lib", "GCC"),
            join(FRAMEWORK_DIR, "variants", board.get("build.variant")),
            join(FRAMEWORK_VARIANT_BCB_DIR, board.get("build.variant"))
        ]
    )

    env.Prepend(
        LIBS=["arm_cortexM0l_math"]
    )
else:
    sys.stderr.write("Unsupported BUILD_SYSTEM %s" % BUILD_SYSTEM)
    env.Exit(1)

#
# Lookup for specific core's libraries
#

env.Append(
    LIBSOURCE_DIRS=[
        join(FRAMEWORK_DIR, "libraries", "__cores__", BUILD_CORE),
        join(FRAMEWORK_DIR, "libraries")
    ]
)

#
# Target: Build Core Library
#

libs = []

if "build.variant" in env.BoardConfig():
    env.Append(
        CPPPATH=[
            join(FRAMEWORK_DIR, "variants", board.get("build.variant")),
            join(FRAMEWORK_VARIANT_BCB_DIR, board.get("build.variant"))
        ]
    )
    libs.append(env.BuildLibrary(
        join("$BUILD_DIR", "FrameworkArduinoVariant"),
        join(FRAMEWORK_DIR, "variants", board.get("build.variant"))
    ))
    
    libs.append(env.BuildLibrary(
        join("$BUILD_DIR", "FrameworkArduinoVariantBCB"),
        join(FRAMEWORK_VARIANT_BCB_DIR, board.get("build.variant"))
    ))

libs.append(env.BuildLibrary(
    join("$BUILD_DIR", "FrameworkArduino"),
    join(FRAMEWORK_DIR, "cores", BUILD_CORE)
))


env.Prepend(LIBS=libs)

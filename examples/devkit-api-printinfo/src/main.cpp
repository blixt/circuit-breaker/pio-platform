#include <Arduino.h>
#include <BCB.h>

void setup() {
  Serial.begin(115200);
  BlixtCB::init();
}

void loop() {
  BlixtCB::printInfo();
  delay(1000);
}